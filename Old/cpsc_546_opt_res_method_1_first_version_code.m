%
% ------------------------------------------------------
% Methods for Optimal Resource Allocation in LTE Uplink
% Code for Method 1
% Author: Naveen Mysore Balasubramanya
%------------------------------------------------------
%
clc;
clear all;
%close all;


randStream = RandStream('mt19937ar','Seed', 2);

M = 10; % Number of Users
N = 24; % Number of RBs

% In the matrices below, each row is a user, each column is a RB

% w is the QoS weights per User
% Begin with a same weights per User
w = ones(M,1);

sMin = 0; %SNRmin = 0dB
sMax = 15; %SNRmax = 15dB

% Begin with a Uniform Distribution of Total Power per User
Pmin = 50; % Min power is 100mW
Pmax = 100; % Max power is 200mW
P = Pmin + (Pmax - Pmin)*rand(randStream, M, 1);

% s is the maximum SINR constraint per RB per User 
s = 10^(sMin/10) + (10^(sMax/10) - 10^(sMin/10))*rand(randStream, M, N);


% Power levels correspodning to s
% Note that maximum power of 20mW results in maximum SNR of 15dB
for i = 1:M
    t(i,:) = s(i,:)*20/(10^(sMax/10)); 
end


% x indicates the fraction of the RB allocated to the User
x = zeros(M,N);


% p is the transmit power
p = zeros(M,N);

% Lagrange Multiplier
lambda = 0.5*ones(M,1)*(min(P)/max(P));
        
mu = zeros(M,N);
MuMax = zeros(1,N);

L_xp_Max = 0;

xOpt = x;
pOpt = p;

for iter = 1:14
    

    % Step 3 - Compute mu and allocate x based on mu
    for n = 1:N

        for i = 1:M

            a = lambda(i);
            b = w(i);
            c = t(i,n);

            g(i,n) = min(max((b/a-1),0), c);
            h(i,n) = log(1 + g(i,n))-(a/b)*g(i,n);


            mu(i,n) = w(i)*h(i,n);

        end

        [MuMax(n)] = max(mu(:,n));

        x(:,n) = 0;

        % Find the user who maximizes the PRB metric
        max_i = [];
        for i = 1:M
            if(mu(i,n) == MuMax(n))
                max_i = [max_i i];
            end
        end
        
        % Assign the PRB to the user who maximizes the metric
        if(length(max_i) == 1)
            x(max_i,n) = 1;

        end
        
        % If more than one user maximizes the PRB metric, randomly assign
        % it to one of the user
        if(length(max_i) > 1)
            randUser = randi(randStream,[1 length(max_i)]);
            x(randUser,n) = 1;
        end
        x(:,n) = max(x(:,n),0);

    end

    % Allocate the power
    p = x.*g;

    % Check for contiguity constraint and if a user doesnt satisfy it,
    % un-allocate all this RBs (Step 4 - first part)
 
    for i = 1:M
        flag1 = 0;
        for n = 1:N-2
            m = 1;
            for k = n+2:N
                if(x(i,n)-x(i,n+1)+x(i,k) > 1)
                    flag1 = 1;
                    break;
                end
            end
            if(flag1 == 1)
                break;
            end
        end
 
        if(flag1 == 1)
            x(i,:) = 0;
        end
    end
    
 
    
    % Re-allocate RBs based on mu (Step 4 - second part)
    for n = 1:N
        while(sum(x(:,n)) ~= 1)
            for j = 1:N
                if(sum(x(:,j)) == 0)
                    [val MuSortInd1] = sort(mu(:,j), 'descend');
                    for k = 1:M
                        x(MuSortInd1(k),j) = 1;
                        p(MuSortInd1(k),j) = x(MuSortInd1(k),j).*g(MuSortInd1(k),j);
                        cantAlloc = 0;
                        for l = 1:N-2
                            for q = l+2:N
                                if((sum(x(:,j)) ~= 1) || (x(MuSortInd1(k),l)-x(MuSortInd1(k),l+1)+x(MuSortInd1(k),q)) > 1) %|| (sum(p(MuSortInd1(k),:)) > P(MuSortInd1(k))))
                                    x(MuSortInd1(k),j) = 0;
                                    p(MuSortInd1(k),j) = 0;
                                    cantAlloc = 1;
                                    break;
                                end
                            end
                            if(cantAlloc == 1)
                                break;
                            end
                        end
                        if((cantAlloc == 0) && (sum(p(MuSortInd1(k),:)) <= P(MuSortInd1(k))))
                            x(MuSortInd1(k),j) = 1;
                            break;
                        end
                    end
                end
            end
        end
    end
     

    % Calculate the total throughput
    L_xp = 0;
    for m = 1:M
        for n = 1:N
            if(x(m,n) ~= 0)
                L_xp = L_xp + w(m)*x(m,n)*log(1+p(m,n)/x(m,n));
            end
        end
        
    end
    
    % Calculate the difference between available power and allocated power
    Psum = sum(P - sum(p')');
    
    
    % This is done to ensure that x andp is chosen at each iteration is
    % better than the ones found till now - Pocket the best solution
    storeLxp(iter) = L_xp;
    storePsum(iter) = Psum;
    
    if(L_xp >= L_xp_Max)
        xOpt = x;
        pOpt = p;
        storeLxp(iter) = L_xp;
        storePsum(iter) = Psum;
        L_xp_Max = L_xp;
    else
        storeLxp(iter) =  L_xp_Max;
        storePsum(iter) = sum(P - sum(pOpt')');
    end
    

    
    stepLamda = 2e-3/iter;
    
    lambda_prev(:,iter) = lambda;
    lambda = lambda - stepLamda.*(P - sum(p')');
    lambda(lambda < 0) = 0;
        
   
end

if(M < 5)
    plotpattern = 'k-s';
else
    if (M >= 5 && M < 10)
        plotpattern = 'k-+';
    else
        plotpattern = 'k-*';
    end
end

figure(1);
plot(storeLxp, plotpattern ,'LineWidth',2);
hold on;
grid on;
xlabel('Iteration Number');
ylabel('Net Throughput');
title('Net Throughput v/s Iteration');
legend(['M = 2 users and N = 4 PRBs'], ['M = 5 users and N = 10 PRBs'], ['M = 10 users and N = 24 PRBs']);

figure(2);
plot(storePsum, plotpattern,'LineWidth',2);
hold on;
grid on;
xlabel('Iteration Number');
ylabel('Difference in total available power and allocated power');
title('Differnce in total available power and allocated power v/s Iteration');
legend(['M = 2 users and N = 4 PRBs'], ['M = 5 users and N = 10 PRBs'], ['M = 10 users and N = 24 PRBs']);