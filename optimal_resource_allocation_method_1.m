%
% ------------------------------------------------------
% Methods for Optimal Resource Allocation in LTE Uplink
% Code for Method 1 - Lagrangian Dual and sub-gradient
% Author: Naveen Mysore Balasubramanya
%------------------------------------------------------
%
clc;
clear all;
close all;


randStream = RandStream('mt19937ar','Seed', 2);

% M = Number of Users
% N = Number of RBs



sMin = -5; %SNRmin = -5dB
sMax = 20; %SNRmax = 20dB

% Begin with a Uniform Distribution of Total Power per User
Pmin = 50; % Min power is 50mW
Pmax = 100; % Max power is 100mW
        
numIter = 25; % Number of iterations
count  = 1;

for M = [2 5 10]
    for N = [2*M 2*M+2 2*M+4] 
        
        % w is the QoS weights per User
        % Begin with a same weights per User
        w = ones(M,1);

        B = [];
        for k = N-2:-1:1
            B = [B; zeros(k,(N-2)-k) ones(k,1) -1*ones(k,1) eye(k)];
        end

        % In the matrices below, each row is a user, each column is a RB
        P = Pmin + (Pmax - Pmin)*rand(randStream, M, 1);

        % s is the maximum SINR constraint per RB per User 
        s = 10^(sMin/10) + (10^(sMax/10) - 10^(sMin/10))*rand(randStream, M, N);


        % Power levels correspodning to s
        % Note that maximum power of 20mW results in maximum SNR of 15dB
        t = s;
        for m = 1:M
            t(m,:) = (s(m,:)*20/(10^(sMax/10))); 
        end


        % x indicates the fraction of the RB allocated to the User
        x = zeros(M,N);


        % p is the transmit power
        p = zeros(M,N);

        % Lagrange Multiplier
        lambda = ones(M,1)*(min(P)/max(P))*(M/N);

        mu = zeros(M,N);
        MuMax = zeros(1,N);


        L_xp_Max = 0;

        xOpt = x;
        pOpt = p;

        f = zeros(M,(N-2)*(N-1)/2);
        penalty_val = 100;

        for iter = 1:numIter

            mu = zeros(M,N);
            g = zeros(M,N);
            h = zeros(M,N);
            
            % Step 3 - Compute mu and allocate x based on mu
            for n = 1:N

                for m = 1:M
                    
                    % Assume that this user gets this PRB
                    x(m,n) = 1;
                    
                    % Compute the penalty
                    for k = 1:(N-2)*(N-1)/2
                        f(m,k) = x(m,:)*B(k,:)'; 
                    end

                    f(f <= 1) = 0;
                    f(f >  1) = penalty_val;
                    RBpenalty = ones(1,(N-2)*(N-1)/2)*f(m,:)';

                    % Compute mu
                    a = lambda(m);
                    b = w(m);
                    c = t(m,n);

                    g(m,n) = min(max((b/a-1),0), c);
                    h(m,n) = log2(1 + g(m,n))-(a/b)*g(m,n);


                    mu(m,n) = max(w(m)*h(m,n)- RBpenalty, 0); 
                end


                x(:,n) = 0;

                % Find the user who maximizes the PRB metric
                [MuMax(n) max_ind] = max(mu(:,n));

                % Assign the PRB to the user who maximizes the metric
                x(max_ind,n) = 1;


            end


            % Allocate the power
            p = x.*g;

            % Adjust the power if the power constraint is violated            
            Pcheck = sum(p,2);          
            for m = 1:M
                if(Pcheck(m) > P(m))
                    Pexcess = sum(p(m,:)) - P(m);
                    allocRB = find(p(m,:) > 0);
                    p(m,allocRB) = min((P(m)-1e-6)/length(allocRB), t(m,allocRB));
                end
            end

%             % ------- Debug code --------------------------------------------------
%             % Check for power constraint violation
%             Pcheck = sum(p,2);
%             for m = 1:M
%                 if(Pcheck(m) > P(m))
%                    disp(['Power constraint violated for ' num2str(M) ' users ' num2str(N) ' PRB on iteration ' num2str(iter)]); 
%                 end
%             end
%             
%             % Check for contiguity constraint for debugging
%             for m = 1:M
%                 flag1 = 0;
%                 for j = 1:N-2
%                     for k = j+2:N
%                         if(x(m,j)-x(m,j+1)+x(m,k) > 1)
%                             disp(['Adjacency constraint violated for ' num2str(M) ' users ' num2str(N) ' PRB on iteration ' num2str(iter)]);
%                         end
%                     end
%                 end
%             end
%             % ------- Debug code --------------------------------------------------

            % Calculate the total throughput
            L_xp = 0;
            for m = 1:M
                for n = 1:N
                    if(x(m,n) ~= 0)
                        L_xp = L_xp + w(m)*x(m,n)*log2(1+p(m,n)/x(m,n));
                    end
                end

            end

            % Calculate the difference between available power and allocated power
            Psum = sum(P - sum(p')');

            % Store the throughput and difference in power
            storeLxp(iter) = L_xp;
            storePsum(iter) = Psum;

            % This is done to ensure that x andp is chosen at each iteration is
            % better than the ones found till now - Pocket the best solution    
            if(L_xp >= L_xp_Max)
                xOpt = x;
                pOpt = p;
                storeLxp(iter) = L_xp;
                storePsum(iter) = Psum;
                L_xp_Max = L_xp;
            else
                storeLxp(iter) =  L_xp_Max;
                storePsum(iter) = sum(P - sum(pOpt')');
            end
    

            % Update lambda
            stepLamda = 2e-3/iter;
            lambda = lambda - stepLamda.*(P - sum(p')');
            lambda(lambda < 0) = 0;

        end

        % Calculate the total throughput
        L_xp = 0;
        for m = 1:M
            for n = 1:N
                if(x(m,n) ~= 0)
                    L_xp = L_xp + w(m)*x(m,n)*log2(1+p(m,n)/x(m,n));
                end
            end

        end

        % Calculate the difference between available power and allocated power
        Psum = sum(P - sum(p')');

        disp(['Throughput = ' num2str(L_xp)]);
        
        NumUsers(count) = M;
        NumPRBs(count) = N;
        storeFinalThroughput(count,:) = storeLxp;
        count = count + 1;
        
    end
end

% Generate the plots
[UserVal] = unique(NumUsers);
plotPattern = ['b-o'; 'r-+'; 'k-x'; 'y--'; 'c-.'];
for i = 1:length(UserVal)
        figure(i);
        UserInd = find(NumUsers == UserVal(i));
        for j = 1:length(UserInd)
            hold on;
            grid on;
            plot(1:numIter, storeFinalThroughput(UserInd(j),:), plotPattern(j,:),'Linewidth',2);
            legendString{j} = (['N = ' num2str(NumPRBs(UserInd(j))) ' PRBs']);
        end
        xlabel('Iteration Number');
        ylabel('Net Throughput');
        title(['Net Throughput for M = ' num2str(NumUsers(UserInd(1))) ' Users']);
        legend(legendString);
end
